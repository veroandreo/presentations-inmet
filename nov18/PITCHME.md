### Geotecnologías, sensado remoto y modelos de distribución de especies para aplicaciones en Salud Pública
<br>
Encuentro Noviembre 2018
<br>
Dra. Verónica Andreo
<br>
@snap[south]
<br>
<img src="intro-inmet/img/logo_conicet_inmet.png" width="60%">
@snapend

---

## Agenda

- Brote LC Corrientes
- Brotes LC Caimancito/Yuto y en NOA/Bolivia
- LV/LC y cambio de cobertura/uso
- Aprendizaje y puesta a punto de técnicas en CONAE
- Presentación introductoria
- Otros

---

### 1. Corrientes

- 1 caso de LV notificado en Sep 2015
- 25 casos de LC notificados entre Oct 2015 y Mar 2016
- Muestreos de flebótomos en Feb 2016 (*N. neivai* sp mas abundante)

---

<img src="nov18/img/corrientes.png" width="80%">

@size[22px](Distribución de casos de LC y proporción de spp de flebótomos)

---

<img src="nov18/img/corrientes_size.png" width="80%">

@size[22px](Distribución de casos de abundancia total de flebótomos en sitios con casos de LC)

---
@snap[north span-100]
Búsqueda de imágenes satelitales (GEE)
@snapend

@snap[midpoint span-100]
![corrientes gee](nov18/img/corrientes_ndvi_gee.png)
@snapend

@snap[south span-100]
@size[22px](https://code.earthengine.google.com/04266d8276100e6711c1c93f664846a4)
@snapend

---

@snap[north span-100]
Visualización capa Global Forest Watch
@snapend

@snap[south span-100]
@size[22px](https://code.earthengine.google.com/04266d8276100e6711c1c93f664846a4)
<br>
![corrientes gee gfw](nov18/img/corrientes_gfw_gee.png)
<br>
@size[22px](Basado en <a href="http://earthenginepartners.appspot.com/science-2013-global-forest/download_v1.5.html">Hansen et al 2013</a>)
@snapend

---

@snap[north span-100]
Inundación en 2015-2016 (SIG Corrientes)
@snapend

@snap[south span-100]
![ide corrientes](nov18/img/corrientes_ide.png)
<br>
@size[22px](http://gis.ciudaddecorrientes.gov.ar/idemcc/)
@snapend

---

### 2. Caimancito (brotes LC NOA y Bolivia 2017-2018)

- número de casos, coordenadas y fechas (?)
- búsqueda de imágenes GEE - GFW

---

@snap[midpoint span-100]
![gfw caimancito](nov18/img/caimancito_gfw_gee.png)
<br>
@size[22px](https://code.earthengine.google.com/f0a42be60eee6d5eba3ee05f5f9c9ffd)
@snapend

---

### 3. LV/LC y cambio de cobertura/uso

- Escala regional NOA/NEA/Centro
  - Datos de casos u ocurrencias del vector?
  - Objetivo: Comprobar si existe una asociación espacial y temporal entre
cambios en la cobertura y brotes o aparición/ocurrencia del vector

---

@snap[midpoint span-100]
![cci](nov18/img/cci.png)
<br>
@size[22px](http://maps.elie.ucl.ac.be/CCI/viewer/index.php)
@snapend

---

### 3. LV/LC y cambio de cobertura

- Escala de localidades para las fechas de algunos brotes
  - Clasificar un par de momentos (agua, bosque y ciudad)
  - Comparar: que aumentó y que disminuyó, dónde ocurren los casos y cuándo
  - Clasif no supervisada, generar info extra, SDM (?)
  - Clustering puntos / Clustering localidades (patrones espaciales de LULC)

*muy pocos puntos, área muy pequena*

---

### 4. ... mientras tanto en CONAE

- Aprendiendo técnicas nuevas y refrescando otras
  - Clustering de series de tiempo (DTW)
  - Procesamiento de imágenes SPOT
  - Incursiones en Machine Learning (Random Forest, K-neighbors)
  - Clasificaciones Supervisadas y No supervisadas - SDM
- Caso de estudio: 
  - Ovitrampas en la ciudad de Córdoba (150 ovitrampas, muestreos semanales, 2017-2018)

---

### 5. Presentación para INMeT

https://gitpitch.com/veroandreo/presentations-inmet/master?p=intro-inmet&grs=gitlab#/

---

### 6. Otros (1)

Done:
- Taller proyectos de investigación (UNRC)
- Participación y Charla en la Escuela de verano GEOSTAT 2018, Praga
- Dictado Curso de GRASS GIS (UNRC)
- Mini-taller de GEE (INTA-UNC)

---

### 6. Otros (2)

Ongoing/to-do:
- Dictado de Minicurso GRASS GIS (12-14/12 - UNC) 
- Revisión trabajo roedores y S2
- Paper dinámica de poblaciones
- Paper Active Fire (ITC - RMIT)
- Paper mapa riesgo dengue y clasificación series de tiempo ovitrampas (MinSal Cba)
- Colaboracion Michael Marshal (ITC)
- Direccion Laura Balparda (Rosario)
- Co-direccion Juan Pinotti (con R. Gonzalez-Ittig)

---

### 6. Otros (3)

Upcoming for 2019:
- FOSS4G Rumania
- Invitación Escuela de verano GEOSTAT 2019, Muenster

---

<br><br><br>
Gracias por su atención :)
<br><br><br><br><br>
@size[16px](Presentation powered by)

<a href="https://gitpitch.com/">
<img src="intro-inmet/img/gitpitch_logo.png" width="20%"></a>


<!--- ver emails entre Daniel y yo 

Bueno, en todo caso, existe alguna base mas completa sobre brotes de leishmaniasis en Argentina y países limítrofes (lugares, fechas, números de casos, donde se han hecho muestreos del vector) como para tener una mejor imagen de la historia de la leishmaniasis en el cono sur? Sin datos reales me siento un poco perdida. Quisiera poder ver la progresión espacial y temporal en mapas para pensar en términos de cambios ambientales y tendencias. 

Los datos de leishmaniasis, para el nivel de definición que necesitamos, los vamos consiguiendo a cuentagotas y después de mucho reclamar (como en el caso de Corrientes que sigue el reclamo). De todas maneras el enfoque es fundamentalmente epidemiológico donde hay mucha información, y las escalas son muy diferentes, en LV donde hay una posible escala continental y en LC donde la escala es focal o como máximo de eco-región, e intervienen factores socio-demográficos, ambientales y climáticos en partes iguales. Alguna de esa información incluso la generamos nosotros así que podemos ir ajustando algunas preguntas. la semana que viene voy a Salvador Mazza y hoy recibí un llamado por el brote sostenido en Aguas Blancas, donde me reclaman medidas de control y siempre digo que hay que hacer el estudio de foco primero.. Por excperiencia, en el caso de los brotes de LC como Salta, Jujuy, Corrientes, ciudades espejo con Bolivia, Brasil-Umuarama, lo es ver en antes-después del brote si hubo cambios destacables en bordes, deforestacion, inundación o nuevos asentamientos,  mostrando eso se facilita después el pedido de ajuste de puntería con los datos que todos son muy gatofloros para compartirlos.

Entiendo que es súper difícil el tema de conseguir los datos (lo he sufrido en carne propia), pero alguna referencia espacial y temporal es necesaria para orientar la búsqueda. Pensé que a INMeT como parte del ministerio de Salud no le sería tan complejo como al resto de los simples mortales acceder a los datos de los casos al menos. Uno de los objetivos del proyecto era armar una base de datos (en retrospectiva especialmente) para colectar toda la info en un lugar y poder hacer análisis acoplados al sensado remoto y los GIS. Existe algún lugar desde donde comenzar a construir tal base? Vigilancia epidemiologica, por ejemplo?

En especial porque el procesamiento y análisis de series de imágenes tipo Landsat o Sentinel en busca de todos y cada uno de esos tipos de eventos ambientales insume una gran cantidad de tiempo como para buscar a ciegas. Alguna verdad de terreno espacial y temporal (ya sea de casos o de eventos ambientales) hace falta para guiar dicha búsqueda.

Hola Vero, sigo cazando brotes de leishmanaisis cutánea, especialmente este de Bolivia-Salta-Jujuy de los últimos años y 2017-2018. De Jujuy ya me pasaron una base de datos que hay que limpiar un poco y pedí que me localicen los casos. Este mail abajo y el adjunto que me mandaron es lo de Caimancito. Aunque no están geo-referenciados quizás sería bueno poder ver los cambios cronológicos con referencia históricas al menos de 2015-2016 en lo que llaman asentamientos nuevos, una vez con esas imágenes (NDVI, NDWI, LST?) para después asociar a casos-tiempo-lugar y levantar hipótesis de modificación ambiental, borde y % en buffer de cada caso en momento probable de infección,

Hola Vero, estoy comenzando a rescatar los datos de un brote de leishmaniasis cutánea que habría comenzado en 2015 en Corrientes. Por los que tengo hasta ahora hay algunos posibles focos iniciales
Corrientes capital: barrios Río Paraná, San Roque, Juan XXIII
Riachuelo: barrios Once Leones y Santa Margarita

Si te interesa sería bueno ver, en forma preliminar, si hubo alguna modificación ambiental registrable por SR en el otoño-verano 2015, seguirla hasta 2016 y controlarla con 2014. Después veremos condiciones climáticas regulares o excepcionales y sitios georeferenciados específicos cuando complete los datos.

Por otro lado Jujuy, Salta y sur Bolivia están cursando un brote de leishmaniasis cutánea, quizás haga el trabajo en terreno pero ahora también pedí datos. La última vez comenzó con un bruto desmonte en La Porcelana, en la frontera de Bolivia y Argentina.

Entre Corrientes y Riachuelo, las dos fuentes de casos, hay unos 15-17 km de centro a centro, están relativamernte cerca (según la escala de análisis), el buffer no puede ir mucho más allá a menos que tengamos algún sitio asociado a trabajo de riego, pero hay casos de sospecha peridomésticos (niños) y posibles movidas para nuevos barrios o en la costa (donde debermos mirar especialmente). El primer caso de este brote registrado, por anamnesis, dice que la lesiòn comenzó en 4/2015, lo que sumado a ciclo de incubación hace que debamos comenzar en el verano 2014-2015 pero enfocando en fin de verano-otoño, ya que el pico 2015 se dio a mitad de año a lo que hay que sumar el tiempo de incubación. Cuando ademñas tenga georeferencias o aunque sea mapa con chinches te aviso
--->
