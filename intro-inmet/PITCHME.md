---?image=intro-inmet/img/mapa_dengue_opacity.png&size=cover

### Geotecnologías, sensado remoto y modelos de distribución de especies para aplicaciones en Salud Pública
<br>
Dra. Verónica Andreo
<br>
@snap[south]
<br>
<img src="intro-inmet/img/logo_conicet_inmet.png" width="60%">
@snapend

---
@transition[none]

@snap[west]
@css[bio-about](Lic. y Dra. Cs. Biológicas - UNRC<br>Mgter. en Aplicaciones Espaciales de Alerta y<br>Respuesta Temprana a Emergencias - UNC<br>Aplicaciones de RS & GIS en Eco-epidemiología<br><br><i>Keywords:</i> RS, GIS, Time series, SDM,<br>Disease Ecology, Rodents, Hantavirus)
<br><br>
@css[bio-about](Entusiasta y defensora del software libre<br><a href="https://grass.osgeo.org/">GRASS GIS</a> Dev Team<br><a href="https://www.osgeo.org/">OSGeo</a> Charter member)
@snapend

@snap[east]
@css[bio-headline](About me)
<br><br>
![myphoto](intro-inmet/img/vero_round_small.png)
<br><br>
@css[bio-byline](@fa[gitlab pad-fa] veroandreo @fa[twitter pad-fa] @VeronicaAndreo<br>@fa[envelope pad-fa] veroandreo@gmail.com)
@snapend

---?image=intro-inmet/img/Paracatu-River_Brazil.jpg&size=cover

@transition[none]

@snap[south-east]
<h2 style="color:white">Sistemas de Información Geográfica</h2>
@snapend

+++

### Qué es un SIG?

Un sistema de hardware, software y procedimientos diseñado para realizar
la captura, almacenamiento, manipulación, análisis, modelización y 
presentación de datos referenciados espacialmente para la resolución de
problemas complejos de planificación y gestión.

+++

@transition[none]

@snap[west span-60]
Un GIS descompone la realidad en temas, i.e., en distintas capas o 
estratos de información de la zona que se desea estudiar
<br><br>
<b>Gran ventaja:</b>
<br>
Permite relacionar y combinar las distintas capas entre sí, para 
contestar preguntas complejas y/u obtener nueva información.
@snapend

@snap[east span-40]
![gis layers](intro-inmet/img/GIS_layers.jpg)
@snapend

+++

### Tipos de datos geográficos

2 formatos o modelos de datos para representar la realidad:
<br><br>
![raster vs vector format](intro-inmet/img/raster-vector.png)

+++

### Atributos de los datos geográficos

@ul
- @color[#8b743d](Espacial): delimitación espacial de cada uno de los objetos geográficos.
- @color[#8b743d](Temático): atributos asociados a una localización u objeto (feature).
- @color[#8b743d](Temporal): fecha absoluta o relativa.
@ulend 

+++

### Preguntas que se pueden responder con los SIG

- Ubicación: ¿Dónde se encuentra... ? 
- Tendencias: ¿Qué ha cambiado desde... ?
- Patrones: ¿Qué patrones espaciales existen... ?
- Proximidad: ¿Cómo es el área alrededor de... ?
- Redes: ¿Cuál es la mejor ruta entre A y B?
- Operaciones lógicas: ¿Dónde ocurre la combinación de las condiciones C y D?
- Temporal: ¿Cuándo ocurren ciertas condiciones?
- Modelos/Simulaciones: ¿Qué pasaría si... ?

---?image=intro-inmet/img/satellite_and_earth.jpg&size=cover

@transition[none]

@snap[south-east]
<h2 style="color:white">Sensado Remoto o Teledetección</h2>
@snapend

+++

### Qué es la teledetección?

Técnica de adquisición y posterior tratamiento de datos de la superficie
terrestre desde sensores instalados en plataformas espaciales, en virtud
de la interacción electromagnética existente entre la tierra y el sensor,
ya sea que la fuente de radiación provenga del sol (teledetección
pasiva) o del propio sensor (teledetección activa).

+++

@transition[none]

@snap[north span-100]
<h3>Componentes del sistema</h3>
@snapend

@snap[west span-50]
<img src="intro-inmet/img/sistema_sensores_remotos.jpg" width="90%">
@snapend

@snap[east span-50 text-08]
@ol(false)
- Fuente de energía 
- Interacción con la atmósfera
- Interacción con la superficie
- Sensor remoto
- Transmisión a otro satélite o estación terrena
- Almacenamiento de datos
- Procesamiento y análisis
@olend
@snapend

+++

@transition[none]

@snap[west]
<h3>Diferentes tipos de resolución</h3>
@snapend

@snap[east]
@ul
- Espacial
- Temporal
- Espectral
- Radiométrica
@ulend
@snapend

+++

#### Resolución espacial

<img class="plain" src="intro-inmet/img/res_espacial.jpg">

+++

#### Resolución espacial

<img class="plain" src="intro-inmet/img/comparison_modis_s2_res.png">

+++

#### Resolución temporal o tiempo de revisita

[https://www.smn.gob.ar/satelite_new](https://www.smn.gob.ar/satelite_new)

+++

#### Resolución espectral

![res espectral](intro-inmet/img/spectral_resolution.png)

+++

#### Resolución espectral

![res espectral](intro-inmet/img/spectral_resolution2.png)

+++

#### Resolución radiométrica

![res radiometrica](intro-inmet/img/res_radiometrica.jpg)

+++

#### Relación de compromiso entre las diferentes resoluciones

<img class="plain" src="intro-inmet/img/all_resolutions_relation.png">

+++

### Ventajas de la teledetección

- Visión global
- Observación a distintas escalas
- Cobertura frecuente y sistemática
- Homogeneidad en la adquisición
- Regiones visibles y no visibles del espectro (diferentes propiedades físicas de las coberturas)
- Formato digital que permite su *fácil* ingestión, procesamiento y análisis 
- Gran disponibilidad de datos libres

+++

### Problemas/Desventajas

- Compromiso entre diferentes resoluciones
- VHR, LiDAR, UAV, Hyper-spectral (**U$S**)
- Datos faltantes, nubes, sombras (**interpolaciones**)
- Necesidad de correcciones de diversos tipos
- Grandes volúmenes de datos vs ancho de banda y capacidad de almacenamiento y cómputo (**cloud computing, paralelización, tiempo de aprendizaje y U$S**)

@css[message-box](Nunca va a reemplazar los datos de campo!)

---

## Modelos de distribución de especies (SDM o ENM)

![sdm workflow](intro-inmet/img/workflow_sdm.png)
<br>
@size[22px](Modelos estadísticos, Machine Learning, técnicas basadas en SIG, etc.)

---

### La combinación de estas herramientas presenta gran potencial para diferentes aplicaciones

+++

- Detección de agua y estimaciones de calidad
- Simulación de incendios e inundaciones
- Estimación de biomasa y diversidad biológica
- Estimación de área quemada y severidad de fuego
- Detección y mapeo de especies invasoras
- Detección, mapeo y cuantificación de área cultivada
- (re)Ubicación de puntos de venta
- Detección y cuantificación de cambios
- Planificación del uso del suelo, planificación urbana
- etc...

---?image=intro-inmet/img/map_world_disease.png&position=center&size=auto 80%

+++?image=intro-inmet/img/healthmap.png&position=center&size=auto 80%

+++

## Geografía de la Salud

@ol[text-08]
- **@color[#8b743d](Ecología de las enfermedades)**: estudio de la distribución espacial de condiciones meteorológicas, biológicas y culturales asociadas con las enfermedades with disease.
- **@color[#8b743d](Provisión de servicios de Salud)**: Patrones espaciales en la provisión de servicios de salud y comportamiento de los pacientes, desigualdades en la provisión de servicios de salud.
- **@color[#8b743d](Ambiente y Salud)**: evaluación del riesgo ambiental, impactos físicos y psico-sociales de la contaminación.
@olend

+++

El principal objetivo al estudiar le ecología de las enfermedades es **@color[#8b743d](comprender)** la influencia de factores ambientales y **@color[#8b743d](predecir)** cuándo y dónde es más probable que una dada enfermedad ocurra

<br>@fa[angle-double-down fa-3x fa-green]<br>

*toma de decisiones, planificación de acciones de prevención, manejo y/o respuesta, etc.*

+++

Para comprender lo que la teledetección y los SIG pueden aportar en el campo de la ecología de enfermedades
<br><br><br>
### Epidemiología Panorámica o Eco-epidemiología

+++

@transition[none]

@snap[west graphql-arch]
<img src="intro-inmet/img/lambin.png" width="89%">
@snapend

@snap[east graphql-bullets-small]
@ol
- Landscape attributes may influence level of transmission of an infection
- Spatial variations in disease risk depend on spatial configuration
- Disease risk depends on the connectivity of habitats
- Landscape is a proxy for specific assoc of hosts and vectors
- Different pathways of pathogen transmission
- The emergence and distrib of inf is controlled by diff factors at multiple scales
- Landscape and meteorological factors control dynamics of risk
- Spatial variation in risk depends on LULC - probability of contact
- The relationship between LU & prob of contact is influenced by land ownership
- Human behaviour is a crucial controlling factor
@olend
@snapend

@snap[south]
@size[22px](Fuente: Lambin et al., 2010.)
<br><br>
@snapend

+++

<img class="plain" src="intro-inmet/img/lambin_rs_env.png">

+++

<img class="plain" src="intro-inmet/img/lambin_rs_env_soc.png">

+++

En general, lo que la teledetección y los SIG puedan aportar dependerá de...
<br><br>
1. Objetivo del estudio
2. Enfermedad bajo estudio, i.e., biología y ecología de vectores, huéspedes y patógenos
3. Escala espacial y temporal (resolución y extensión)
4. Datos de los que dispongamos
5. Datos ambientales de los que dispongamos o necesitemos de acuerdo a la escala espacial y temporal

+++

@snap[north-west span-100 text-center]
@size[20px](Diferentes factores afectan la distribución de las especies en diferentes escalas espaciales. Pearson & Dawson, 2003.)
<br>
<img src="intro-inmet/img/env_vars_and_scale.png" width="600px"> 
@snapend

@snap[south-west span-100 text-center]
@size[20px](Escalas espaciales y temporales en el movimiento de animales y la teledeteccion. Neumann et al., 2015.)
<br>
<img src="intro-inmet/img/ecology_and_rs_scale.png" width="600px"> 
@snapend

+++

### Aplicaciones de la teledetección y los SIG en Geografía de la Salud

- Espacialización de brotes (patrones and causas)
- Mapeo de habitat (potencial) de huéspedes y vectores
- Estimación de índices ambientales y monitoreo
- Mapeo del riesgo de enfermedad
- Análisis de paisaje: distintas métricas, fragmentación
- Emplazamiento de servicios de salud y localización de las mejores rutas a hospitales

---

### Un paso más...

+++?image=intro-inmet/img/mapa_dengue_opacity.png&size=cover

... a la web: 
<br><br>
## webGIS

+++?image=intro-inmet/img/mapa_dengue_opacity.png&size=cover

- carga de datos de casos, huéspedes, vectores y patógenos por parte de Ministerios de Salud y otras instituciones
- captura y procesamiento de datos satelitales 
- captura y procesamiento de datos geoespaciales
- predicción de riesgo

---

Entonces, volviendo a mi proyecto...

<br><br>

### @color[#8b743d](Geo-tecnologías, sensado remoto y modelos de distribución de especies para aplicaciones en Salud Pública)

+++

### La pregunta
<br>
@color[#8b743d](Cuáles son los determinantes ambientales del riesgo?)
<br><br>
*@size[20px](me interesan especialmente aquellos que se puedan detectar, extraer o inferir a partir de imágenes de satelite, fotos aéreas o drones)*

+++

### Objetivo general
<br>
*Diseño y desarrollo de una plataforma utilizando tecnologías geo-espaciales para el monitoreo y la predicción de áreas de riesgo de enfermedades zoonóticas en Argentina con el fin último de mejorar la vigilancia y orientar las acciones en Salud Pública.*

+++

@transition[none]

@snap[north]
<br>
<h3>Objetivos específicos</h3>
@snapend

@snap[west objectives text-center fragment]
1. Capas de variables ambientales
@snapend

@snap[midpoint objectives text-center fragment]
2. Capas de información socio-económica
@snapend

@snap[east objectives text-center fragment]
3. Base de datos geo-referenciados
@snapend

@snap[south-west objectives text-center fragment]
4. Mapas de (probabilidad de) ocurrencia
@snapend

@snap[south objectives text-center fragment]
5. Mapas de riesgo
@snapend

@snap[south-east objectives text-center fragment]
6. Automatización de procesos
@snapend

+++

### Dónde empezar?

- Definir la extensión espacial y temporal de interés
- Buscar datos geoespaciales y de teledetección disponibles y apropiados
- Preparación, pre-procesamiento y procesamiento de datos 
- Extracción/obtención de variables relevantes

+++

### Su contribución es fundamental
<br>
**@color[#8b743d](con datos de huéspedes, vectores y/o patógenos referenciados espacial y temporalmente)**

+++

## Para qué?

@ul
- entender qué variables ambientales observables desde herramientas de teledetección pueden predecir su ocurrencia en espacio y tiempo
<br>
- asociarlos a eventos conocidos como inundaciones, sequías, cambios en uso de la tierra, etc. y determinar tiempo y distancia a altos niveles de riesgo
@ulend

+++

@snap[north span-100]
## Ejemplos
<br>
@snapend

@snap[west span-50]
SPH en Argentina
<br>
![Hanta](intro-inmet/img/HPS_south_arg.png)
@snapend

@snap[east span-50]
WNF en Grecia
<br>
![WNV](intro-inmet/img/wnv_cocluster.png)
@snapend

+++

@snap[north span-100]
## Ejemplos

Leishmaniasis cutánea en Corrientes: Cambios en el uso de la tierra y ocurrencia de brote
<br>
@snapend

@snap[south span-100]
<img src="intro-inmet/img/cambios_corrientes.png" width="60%">
@snapend

+++

@snap[north span-100]
## Ejemplos
<br><br>
@snapend

@snap[west span-45 text-left]
@ul[false]
- Patrones temporales y espaciales en *Aedes aegypty* en la ciudad de Córdoba
- Asociación con variables derivadas del análisis de imágenes SPOT (6m res espacial)
@ulend
@snapend

@snap[east span-55]
<br>
![time series](intro-inmet/img/pam_series.png)
<br>
![temp pattern in space](intro-inmet/img/clustering_maps.png)
@snapend

+++

@snap[north span-100 text-left]
## En desarrollo...
@snapend

@snap[west span-40 text-08]
Cadena de procesamiento con imágenes de muy alta resolución espacial para trabajar con datos de áreas urbanas (*aplicable a diferentes vectores y patógenos*)
@snapend

@snap[east span-60]
<img src="intro-inmet/img/obia_example.png" width="70%">
@snapend

---

@snap[west span-50]
Si alguien esta interesad@...
<br><br>
@fa[square light-brown] Sí
<br>
@fa[square light-brown] No
<br>
@fa[square light-brown] Ns/Nc
@snapend

+++

@snap[west span-50]
Si alguien esta interesad@...
<br><br>
@fa[check-square light-brown] Sí
<br>
@fa[square light-brown] No
<br>
@fa[square light-brown] Ns/Nc
@snapend

+++

Esta semana estoy por acá, podemos juntarnos y charlar sobre posibilidades de colaboraciones
<br><br>
@fa[rocket fa-3x fa-green]
<br><br>
@fa[balance-scale] @size[24px](para beneficio de todos y con reglas claras desde el principio) @fa[balance-scale]

---

### Fin último 
<br>
**@color[#8b743d](WebGIS institucional que sirva de herramienta para el planeamiento y la prevención)**

<br>
**@color[#8b743d](y plataforma donde se muestren los temas que se trabajan en el INMeT)**

+++

- Tener todos los datos espaciales en una base de datos común
- Obtener y procesar datos geoespaciales y de teledetección
- Creación de mapas de distribución y/o mapas de riesgo
- Actualización automática con casos/muestreos nuevos y nuevos datos ambientales
- Diferentes niveles de usuarios y permisos

+++

@transition[none]

@snap[north-west span-100 text-08]
Ejemplo: <a href="https://www.vectorbase.org/popbio/map/">https://www.vectorbase.org/popbio/map/</a>
<br>
@snapend

<img src="intro-inmet/img/vectorbase.png" width="95%">

+++

@transition[none]

@snap[north-west span-100 text-08]
Ejemplo: <a href="http://udege-admin.unc.edu.ar/udege-ide/composer/">http://udege-admin.unc.edu.ar/udege-ide/</a>
<br>
@snapend

<img src="intro-inmet/img/udege_example.png" width="95%">

+++
@transition[none]

@snap[north-west span-100 text-08]
Ejemplo: <a href="https://geoportal.conae.gov.ar/geoexplorer/composer/">https://geoportal.conae.gov.ar/geoexplorer/composer/</a>
<br>
@snapend

<img src="intro-inmet/img/dengue_conae_example.png" width="95%">

---

<img src="intro-inmet/img/gummy-question.png" width="37%">

---
<br><br><br>
### Muchas gracias por su atención!!
<br><br><br><br><br>
@size[20px](Presentation powered by)

<a href="https://gitpitch.com/">
<img src="intro-inmet/img/gitpitch_logo.png" width="20%"></a>
